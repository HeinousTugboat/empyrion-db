/* eslint-disable */
interface reducerState {
  currentObject: any | null;
  completedObjects: any[];
}

export function parseSimpleProperty(text: string): any {
  const parts = text.split(',');
  const props = parts.reduce((list: string[], part: string) => {

    const quotes = Array.from(list.join(',').matchAll(/"/g)).length % 2;

    if (quotes === 1) {
      list[list.length - 1] += part;
    } else {
      list.push(part);
    }

    return list;
  }, []);

  if (props.length === 1) {
    const [prop, val] = props[0]!.split(':');
    if (prop === undefined) { throw new Error('Simple Property missing any property! {text}'); }
    return [prop.trim(), val?.trim()];
  }

  const [type, value] = props.shift()!.split(':');
  const propDetails = props.reduce((o, propString) => {
    const [prop, val] = propString.split(':');

    o[prop!.trim()] = val!.trim();

    return o;
  }, {} as any);
  const complexProp = {
    value: value!.trim(),
    ...propDetails
  };

  return [type!.trim(), complexProp];
}

export function parseObjectStart(text: string): any {
  const [, type, ...remainder] = text.split(' ');

  if (type === 'Child') {
    const [childType, ...childRemainder] = remainder;
    const properties = childRemainder.join(' ').split(',').reduce((props, propString) => {
      const [prop, val] = parseSimpleProperty(propString);

      if (prop.trim() === '') { return props; }

      props[prop.trim()] = val?.trim();

      return props;
    }, {} as any);

    return {
      type: childType,
      child: true,
      ...properties
    };
  }

  const properties = remainder.join(' ').split(',').reduce((props, propString) => {
    const [prop, val] = parseSimpleProperty(propString);
    props[prop.trim()] = val?.trim();

    return props;
  }, {} as any);

  return {
    type,
    parent: null,
    ...properties
  };
}

export function parseComment(text: string): string {
  const parts = text.split('#');
  if (parts.length === 0) { throw new Error('split returns nothing?'); }
  if (parts.length === 1) { return text; }
  if (parts.length === 2 && Array.from(parts[0]!.matchAll(/"/g)).length % 2 === 1) { return text; }

  parts.pop();

  return parseComment(parts.join('#'));
}

export function parseLine(rawText: string, base: any): any {
  const text = parseComment(rawText).trim();
  if (text.startsWith('{')) {
    const result = parseObjectStart(text);

    if (result.child) {
      result.parent = base;
      delete result.child;

      if (result.type.length > 1) {
        base[result.type] = result;
      } else {
        base.children = [...base.children ?? [], result];
        delete result.type;
      }

      return result;
    }
    return Object.assign(base ?? {}, result);
  } else if (text.startsWith('}')) {
    const parent = base.parent;
    delete base.parent;
    return parent;
  }

  if (base === null) { return; }

  const [prop, val] = parseSimpleProperty(text.trim());
  base[prop] = val;

  return base;
}

export function parseText(text: string) {
  // Not a huge fan of this, but it works. NB: It also clobbers comments in strings and other legal places
  const strippedBlocks = text.replaceAll(/\/\*(.|\n|\r)*?\*\//gm, '');
  const strippedComments = strippedBlocks.replaceAll(/#.+\n/gm, '\n');

  const lines = strippedComments.split('\n');

  const results = lines.reduce((o: reducerState, rawLine: string) => {
    if (parseComment(rawLine).trim() === '') { return o; }
    const result = parseLine(rawLine, o.currentObject);
    if (o.currentObject !== null && (result === null || result === undefined)) {
      o.completedObjects.push(o.currentObject);
    }

    o.currentObject = result;

    return o;
  }, { currentObject: null, completedObjects: [] });

  if (results.currentObject !== null) {
    console.error('Unfinished object..' + results.currentObject);
  }

  return results.completedObjects;
}

export function doStuff(text: string, templateText: string) {
  const objects = parseText(text);
  const templates = parseText(templateText);

  console.log('results:', objects.length);

  const keys = new Set<string>();
  const inputKeys = new Set<string>();
  const objectMap = new Map<string, any>();
  const categories = new Map<string, any[]>();

  for (const o of objects) {
    if (objectMap.has(o.Name)) { console.log('Object already in map: ', o.Name); }
    objectMap.set(o.Name, o);
    if (o.Category === undefined) { o.Category = 'No Category'; }
    const category = categories.get(o.Category) ?? [];
    categories.set(o.Category, [...category, o]);

    for (const key of Object.keys(o)) {
      keys.add(key);
    }

    if (o.Inputs) {
      for (const input of o.Inputs) {
        input.parent = o.Name;
        for (const key of Object.keys(input)) {
          inputKeys.add(key);
        }
      }
    }
  }

  for (const t of templates) {
    if (objectMap.has(t.Name)) {
      const o = objectMap.get(t.Name) as any;

      if (o.template !== undefined) { console.log('many templates!'); }
      o.template = t;
    }
  }

  calcCosts(templates, objectMap);

  // console.log(categories.get('Ingredients'));
  // console.log(categories.keys());
  // console.log(keys);
  // console.log(inputKeys);
  // console.log(templates);
  // console.log(objects.filter((o) => o.template !== undefined)[1]);
  console.log(objects.filter((o) => o.Id === '7'));
  return {
    objects,
    objectMap,
    categories
  };
}

function calcCosts(templates: any, objectMap: Map<string, any>) {
  for (const t of templates) {
    t.templateCost = 0;
    t.missingCost = [];
    t.missingObject = [];

    if (t.Target === '""') { continue; }

    for (const input of Object.keys(t.Inputs)) {
      if (input === 'type') { continue; }
      if (!objectMap.has(input)) {
        t.missingObject = [...t.missingObject ?? [], input];
        console.log('missing input object!', t.Name, t.missingObject);
        continue;
      }

      const o = objectMap.get(input);

      if (o.MarketPrice?.value === undefined) {
        t.missingCost = [...t.missignCost ?? [], input];
        console.log('input without cost!', t.Name, t.missingCost);
        continue;
      }
      t.templateCost += +o.MarketPrice?.value
    }
  }
}


// TODO: Template Target stripping commas. :-(
