/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { forkJoin } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ParserService } from './parser.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  objects: any[] = [];
  objectMap: Map<string, any> = new Map();
  categories: Map<string, any> = new Map();
  public selectedCategory?: string;
  public sortKey: string = 'profit';
  public mappedItems: any[] = [];
  constructor(private http: HttpClient, private parser: ParserService) {

    console.log(parser);
    const requests = [
      this.http.get('assets/ItemsConfig.ecf', { responseType: 'text' }),
      this.http.get('assets/Templates.ecf', { responseType: 'text' })
    ];

    forkJoin(requests).subscribe(([items, templates]) => {
      const output = parser.doStuff(items!, templates!);
      this.objects = output.objects;
      this.objectMap = output.objectMap;
      this.categories = output.categories;
      this.filterItems();
    });
  }

  public setCategory(category?: string): void {
    this.selectedCategory = category;
    this.filterItems();
  }

  public setSortKey(key: string): void {
    this.sortKey = key;
    this.filterItems();
  }
  public mapItems(): void {
    this.mappedItems = this.objects.map(item => ({
      id: item.Id,
      name: item.Name,
      category: item.Category,
      ref: item.Ref,
      price: item.MarketPrice?.value,
      templateCost: item.template?.templateCost,
      profit: (item.template !== undefined && item.template.Target !== '""') ? ((item.MarketPrice?.value ?? 0) - (item.template?.templateCost ?? 0)) : 0,
      profitPerTime: (item.template !== undefined && item.template.Target !== '""') ? ((item.MarketPrice?.value ?? 0) - (item.template?.templateCost ?? 0)) / item.template.CraftTime : 0,
      hasTemplate: item.template !== undefined && item.template.Target !== '""',
      missingCost: item.template?.missingCost ?? [],
      missingObject: item.template?.missingObject ?? []
    }));
  }
  public filterItems(): void {
    this.mapItems();

    if (this.selectedCategory !== undefined) {
      this.mappedItems = this.mappedItems.filter((item) => item.category === this.selectedCategory);
    }

    this.mappedItems = this.mappedItems.sort((el1, el2) => ((el2[this.sortKey] ?? 0) - (el1[this.sortKey] ?? 0)));
  }
}


