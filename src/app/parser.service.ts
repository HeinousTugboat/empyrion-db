/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';

import * as parser from './parser';

@Injectable({
  providedIn: 'root'
})
export class ParserService {
  public doStuff(items: string, templates: string): {
    objects: any[],
    objectMap: Map<string, any>,
    categories: Map<string, any[]>
  } {
    return parser.doStuff(items, templates);
  }
}
